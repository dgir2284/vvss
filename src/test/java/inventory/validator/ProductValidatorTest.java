package inventory.validator;

import inventory.model.InhousePart;
import inventory.model.Part;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductValidatorTest {
    private static final String PRICE_MUST_BE_GREATER_THAN_ZERO_ERROR_MESSAGE =
            "The price must be greater than $0. ";
    private static final String INVENTORY_LEVEL_MUST_BE_GREATER_THAN_ZERO_ERROR_MESSAGE =
            "The inventory level must be greater than 0. ";
    private static final String MIN_VALUE_MUST_BE_LESS_THAN_MAX_VALUE_ERROR_MESSAGE =
            "The Min value must be less than the Max value. ";
    private static final String PRODUCT_PRICE_MUST_BE_GREATER_THAN_COST_OF_PARTS_ERROR_MESSAGE =
            "Product price must be greater than cost of parts. ";
    private static final String INVENTORY_LEVEL_IS_HIGHER_THAN_THE_MAXIMUM_VALUE_ERROR_MESSAGE =
            "Inventory level is higher than the maximum value. ";
    private String name;
    private double price;
    private int inStock;
    private int min;
    private int max;
    private Part part;
    private ObservableList<Part> parts;
    private String errorMessage;

    @BeforeEach
    void setUp() {
        name = "dummy name";
        price = 1.0;
        inStock = 15;
        min = 11;
        max = 15;
        part = new InhousePart(1, "dummy", 0.0, 0, 0, 0, 0);
        if (parts == null) {
            parts = FXCollections.observableArrayList();
        }
        parts.add(part);
        errorMessage = "";
    }

    @AfterEach
    void tearDown() {
        parts.clear();
        errorMessage = "";
    }

    @Test
    @Order(1)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForPriceALittleLowerThanMinimumAcceptedValue() {
        price = 0.00;
        part.setPrice(-0.1);
        parts.add(0, part);
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(PRICE_MUST_BE_GREATER_THAN_ZERO_ERROR_MESSAGE, actualErrorMessage);
    }

    @Test
    @Order(2)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForPriceEqualWithMinimumAcceptedValue() {
        price = 0.01;
        String expectedErrorMessage = "";
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    @Order(3)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForPriceALittleGreaterThanMinimumAcceptedValue() {
        price = 0.02;
        String expectedErrorMessage = "";
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    @Order(4)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForMinALittleLowerThanMinimumAcceptedValue() {
        min = -1;
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(INVENTORY_LEVEL_MUST_BE_GREATER_THAN_ZERO_ERROR_MESSAGE, actualErrorMessage);
    }

    @Test
    @Order(5)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForMinEqualWithMinimumAcceptedValue() {
        min = 0;
        String expectedErrorMessage = "";
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    @Order(6)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForMinALittleGreaterThanMinimumAcceptedValue() {
        min = 1;
        String expectedErrorMessage = "";
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    @Order(7)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForMinALittleLowerThanMax() {
        min = 14;
        String expectedErrorMessage = "";
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    @Order(8)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForMinEqualWithMax() {
        min = 15;
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(MIN_VALUE_MUST_BE_LESS_THAN_MAX_VALUE_ERROR_MESSAGE, actualErrorMessage);
    }

    @Test
    @Order(9)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForMinALittleGreaterThanMax() {
        min = 16;
        inStock = 16;
        String expectedErrorMessage = MIN_VALUE_MUST_BE_LESS_THAN_MAX_VALUE_ERROR_MESSAGE +
                INVENTORY_LEVEL_IS_HIGHER_THAN_THE_MAXIMUM_VALUE_ERROR_MESSAGE;
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    @Order(10)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForPriceALittleLowerThanTotalSumOfParts() {
        price = 9.99;
        part.setPrice(10.00);
        parts.clear();
        parts.add(0, part);
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(PRODUCT_PRICE_MUST_BE_GREATER_THAN_COST_OF_PARTS_ERROR_MESSAGE, actualErrorMessage);
    }

    @Test
    @Order(11)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForPriceEqualWithTotalSumOfParts() {
        price = 10.00;
        part.setPrice(10.00);
        parts.clear();
        parts.add(0, part);
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(PRODUCT_PRICE_MUST_BE_GREATER_THAN_COST_OF_PARTS_ERROR_MESSAGE, actualErrorMessage);
    }

    @Test
    @Order(12)
    @Tag("BVA")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForPriceALittleGreaterThanTotalSumOfParts() {
        price = 10.01;
        part.setPrice(10.00);
        parts.clear();
        parts.add(part);
        String expectedErrorMessage = "";
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    //    ECP
    @Test
    @Order(13)
    @Tag("ECP")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForPriceAcceptedValue() {
        price = 15.50;
        String expectedErrorMessage = "";
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    @Order(14)
    @Tag("ECP")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForPriceLowerThanMinimumAcceptedValue() {
        price = -5.0;
        part.setPrice(-6.0);
        parts.clear();
        parts.add(part);
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(PRICE_MUST_BE_GREATER_THAN_ZERO_ERROR_MESSAGE, actualErrorMessage);
    }

    @Test
    @Order(15)
    @Tag("ECP")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForPriceLowerThanTotalSumOfParts() {
        price = 5.25;
        part.setPrice(5.26);
        parts.clear();
        parts.add(part);
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(PRODUCT_PRICE_MUST_BE_GREATER_THAN_COST_OF_PARTS_ERROR_MESSAGE, actualErrorMessage);
    }

    @Test
    @Order(16)
    @Tag("ECP")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForMinAcceptedValue() {
        min = 5;
        String expectedErrorMessage = "";
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    @Order(17)
    @Tag("ECP")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForMinLowerThanMinimumAcceptedValue() {
        min = -2;
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(INVENTORY_LEVEL_MUST_BE_GREATER_THAN_ZERO_ERROR_MESSAGE, actualErrorMessage);
    }

    @Test
    @Order(18)
    @Tag("ECP")
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    void validateForMinGreaterThanMax() {
        min = 155;
        inStock = 155;
        String expectedErrorMessage = MIN_VALUE_MUST_BE_LESS_THAN_MAX_VALUE_ERROR_MESSAGE +
                INVENTORY_LEVEL_IS_HIGHER_THAN_THE_MAXIMUM_VALUE_ERROR_MESSAGE;
        String actualErrorMessage = ProductValidator.validate(name, price, inStock, min, max, parts, errorMessage);
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }
}
