package inventory.service;

import inventory.exception.RepositoryException;
import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InventoryServiceTest {
    private InventoryRepository repository;
    private InventoryService service;

    @BeforeEach
    void setUp() {
        repository = mock(InventoryRepository.class);
        service = new InventoryService(repository);
    }

    @Test
    void addInhousePart() throws RepositoryException {
        InhousePart expectedAddedPart =
                new InhousePart(101010, "name", 1d, 5, 1, 10, 0);
        when(repository.getAutoPartId()).thenReturn(101010);
        service.addInhousePart("name", 1d, 5, 1, 10, 0);
        ArgumentCaptor<Part> capturedPart = ArgumentCaptor.forClass(Part.class);
        verify(repository).addPart(capturedPart.capture());
        Part actualAddedPart = capturedPart.getValue();
        assertEquals(expectedAddedPart.getPartId(), actualAddedPart.getPartId());
        assertEquals(expectedAddedPart.getName(), actualAddedPart.getName());
        assertEquals(expectedAddedPart.getPrice(), actualAddedPart.getPrice(), 0.1);
        assertEquals(expectedAddedPart.getInStock(), actualAddedPart.getInStock());
        assertEquals(expectedAddedPart.getMin(), actualAddedPart.getMin());
        assertEquals(expectedAddedPart.getMax(), actualAddedPart.getMax());
    }

    @Test
    void getAllParts() {
        ObservableList<Part> dummyPartList = FXCollections.observableArrayList(
                new InhousePart(1, "name", 1d, 1, 1, 1, 1)
        );
        when(repository.getAllParts()).thenReturn(dummyPartList);
        ObservableList<Part> allParts = service.getAllParts();
        assertEquals(1, allParts.size());
        Part actualPart = allParts.get(0);
        assertEquals(1, actualPart.getPartId());
        assertEquals("name", actualPart.getName());
        assertEquals(1d, actualPart.getPrice(), 0.1);
        assertEquals(1, actualPart.getInStock());
        assertEquals(1, actualPart.getMin());
        assertEquals(1, actualPart.getMax());
    }

    @Test
    void deletePart() throws RepositoryException {
        InhousePart expectedDeletedPart =
                new InhousePart(101010, "name", 1d, 5, 1, 10, 0);
        service.deletePart(expectedDeletedPart);
        ArgumentCaptor<Part> capturedPart = ArgumentCaptor.forClass(Part.class);
        verify(repository).deletePart(capturedPart.capture());
        Part actualDeletedPart = capturedPart.getValue();
        assertEquals(expectedDeletedPart.getPartId(), actualDeletedPart.getPartId());
        assertEquals(expectedDeletedPart.getName(), actualDeletedPart.getName());
        assertEquals(expectedDeletedPart.getPrice(), actualDeletedPart.getPrice(), 0.1);
        assertEquals(expectedDeletedPart.getInStock(), actualDeletedPart.getInStock());
        assertEquals(expectedDeletedPart.getMin(), actualDeletedPart.getMin());
        assertEquals(expectedDeletedPart.getMax(), actualDeletedPart.getMax());
    }
}
