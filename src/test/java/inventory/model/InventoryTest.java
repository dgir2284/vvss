package inventory.model;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class InventoryTest {
    private static Inventory inventory;
    private static Part part1;
    private static Part part5;

    @BeforeEach
    void setUp() {
        inventory = new Inventory();
        part1 = new InhousePart(1, "piesa_unu", 1.0, 5, 1, 10, 1);
        Part part2 = new InhousePart(2, "piesa_unu", 2.0, 5, 1, 10, 1);
        Part part3 = new InhousePart(3, "piesa_unu", 3.0, 5, 1, 10, 1);
        Part part4 = new InhousePart(4, "piesa_unu", 4.0, 5, 1, 10, 1);
        part5 = new InhousePart(5, "piesa_unu", 5.0, 5, 1, 10, 1);
        inventory.addPart(part1);
        inventory.addPart(part2);
        inventory.addPart(part3);
        inventory.addPart(part4);
        inventory.addPart(part5);
    }

    @Test
    @Order(1)
    @Tag("F02_TC02")
    void testSearchForPartUsingPartName() {
        Part actualPart = inventory.lookupPart("piesa_unu");
        assertEquals(part1.getPartId(), actualPart.getPartId());
        assertEquals(part1.getName(), actualPart.getName());
        assertEquals(part1.getPrice(), actualPart.getPrice());
        assertEquals(part1.getInStock(), actualPart.getInStock());
        assertEquals(part1.getMin(), actualPart.getMin());
        assertEquals(part1.getMax(), actualPart.getMax());
    }

    @Test
    @Order(2)
    @Tag("F02_TC03")
    void testSearchForPartUsingPartId() {
        Part actualPart = inventory.lookupPart("5");
        assertEquals(part5.getPartId(), actualPart.getPartId());
        assertEquals(part5.getName(), actualPart.getName());
        assertEquals(part5.getPrice(), actualPart.getPrice());
        assertEquals(part5.getInStock(), actualPart.getInStock());
        assertEquals(part5.getMin(), actualPart.getMin());
        assertEquals(part5.getMax(), actualPart.getMax());
    }

    @Test
    @Order(3)
    @Tag("F02_TC01")
    void testSearchForPartInEmptyPartList() {
        // empty the list
        inventory = new Inventory();
        Part actualPart = inventory.lookupPart("-1");
        assertNull(actualPart);
    }

    @Test
    @Order(4)
    @Tag("F02_TC04")
    void testSearchForPartInListContainingNullValue() {
        // empty the list and add only one null value
        inventory = new Inventory();
        inventory.addPart(null);
        Part actualPart = inventory.lookupPart("dummy");
        assertNull(actualPart);
    }
}
