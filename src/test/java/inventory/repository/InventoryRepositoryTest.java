package inventory.repository;

import inventory.exception.RepositoryException;
import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InventoryRepositoryTest {
    private Inventory inventory;
    private InventoryRepository repository;

    @BeforeEach
    void setUp() {
        inventory = mock(Inventory.class);
        when(inventory.getAllParts()).thenReturn(FXCollections.emptyObservableList());
        when(inventory.getProducts()).thenReturn(FXCollections.emptyObservableList());
        repository = new InventoryRepository(inventory, "dummy/file/path");
    }

    @Test
    void addPart() throws RepositoryException {
        InhousePart expectedAddedPart =
                new InhousePart(101010, "name", 1d, 5, 1, 10, 0);
        ArgumentCaptor<Part> capturedPart = ArgumentCaptor.forClass(Part.class);
        repository.addPart(expectedAddedPart);
        verify(inventory).addPart(capturedPart.capture());
        Part actualAddedPart = capturedPart.getValue();
        assertEquals(expectedAddedPart.getPartId(), actualAddedPart.getPartId());
        assertEquals(expectedAddedPart.getName(), actualAddedPart.getName());
        assertEquals(expectedAddedPart.getPrice(), actualAddedPart.getPrice(), 0.1);
        assertEquals(expectedAddedPart.getInStock(), actualAddedPart.getInStock());
        assertEquals(expectedAddedPart.getMin(), actualAddedPart.getMin());
        assertEquals(expectedAddedPart.getMax(), actualAddedPart.getMax());
    }

    @Test
    void getAllParts() {
        ObservableList<Part> expectedPartList = FXCollections.observableArrayList(
                new InhousePart(1, "name", 1d, 1, 1, 1, 1)
        );
        when(inventory.getAllParts()).thenReturn(expectedPartList);
        ObservableList<Part> actualPartList = repository.getAllParts();
        assertFalse(actualPartList.isEmpty());
        Part actualPart = actualPartList.get(0);
        assertEquals(1, actualPart.getPartId());
        assertEquals("name", actualPart.getName());
        assertEquals(1d, actualPart.getPrice(), 0.1);
        assertEquals(1, actualPart.getInStock());
        assertEquals(1, actualPart.getMin());
        assertEquals(1, actualPart.getMax());
    }

    @Test
    void deletePart() throws RepositoryException {
        InhousePart expectedDeletedPart =
                new InhousePart(101010, "name", 1d, 5, 1, 10, 0);
        ArgumentCaptor<Part> capturedPart = ArgumentCaptor.forClass(Part.class);
        doNothing().when(inventory).deletePart(capturedPart.capture());
        repository.deletePart(expectedDeletedPart);
        Part actualDeletedPart = capturedPart.getValue();
        assertEquals(expectedDeletedPart.getPartId(), actualDeletedPart.getPartId());
        assertEquals(expectedDeletedPart.getName(), actualDeletedPart.getName());
        assertEquals(expectedDeletedPart.getPrice(), actualDeletedPart.getPrice(), 0.1);
        assertEquals(expectedDeletedPart.getInStock(), actualDeletedPart.getInStock());
        assertEquals(expectedDeletedPart.getMin(), actualDeletedPart.getMin());
        assertEquals(expectedDeletedPart.getMax(), actualDeletedPart.getMax());
    }
}
