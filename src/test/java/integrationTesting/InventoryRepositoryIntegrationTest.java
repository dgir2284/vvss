package integrationTesting;

import inventory.exception.RepositoryException;
import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

class InventoryRepositoryIntegrationTest {
    private Inventory inventory;
    private InventoryService service;

    @BeforeEach
    void setUp() {
        inventory = mock(Inventory.class);
        InventoryRepository repository = new InventoryRepository(inventory, "dummy/file/path");
        service = new InventoryService(repository);
    }

    @Test
    @Order(1)
    void addInhousePart() throws RepositoryException {
        InhousePart expectedAddedPart =
                new InhousePart(101010, "name", 1d, 5, 1, 10, 0);
        when(inventory.getAutoPartId()).thenReturn(101010);
        service.addInhousePart("name", 1d, 5, 1, 10, 0);
        ArgumentCaptor<Part> partCaptor = ArgumentCaptor.forClass(Part.class);
        verify(inventory).addPart(partCaptor.capture());
        Part actualAddedPart = partCaptor.getValue();
        assertNotNull(actualAddedPart);
        assertEquals(expectedAddedPart.getPartId(), actualAddedPart.getPartId());
        assertEquals(expectedAddedPart.getName(), actualAddedPart.getName());
        assertEquals(expectedAddedPart.getPrice(), actualAddedPart.getPrice(), 0.1);
        assertEquals(expectedAddedPart.getInStock(), actualAddedPart.getInStock());
        assertEquals(expectedAddedPart.getMin(), actualAddedPart.getMin());
        assertEquals(expectedAddedPart.getMax(), actualAddedPart.getMax());
    }

    @Test
    @Order(2)
    void deletePart() throws RepositoryException {
        InhousePart expectedDeletedPart =
                new InhousePart(101010, "name", 1d, 5, 1, 10, 0);
        service.deletePart(expectedDeletedPart);
        ArgumentCaptor<Part> partCaptor = ArgumentCaptor.forClass(Part.class);
        verify(inventory).deletePart(partCaptor.capture());
        Part actualDeletedPart = partCaptor.getValue();
        assertNotNull(actualDeletedPart);
        assertEquals(expectedDeletedPart.getPartId(), actualDeletedPart.getPartId());
        assertEquals(expectedDeletedPart.getName(), actualDeletedPart.getName());
        assertEquals(expectedDeletedPart.getPrice(), actualDeletedPart.getPrice(), 0.1);
        assertEquals(expectedDeletedPart.getInStock(), actualDeletedPart.getInStock());
        assertEquals(expectedDeletedPart.getMin(), actualDeletedPart.getMin());
        assertEquals(expectedDeletedPart.getMax(), actualDeletedPart.getMax());
    }

}
