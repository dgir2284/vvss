package integrationTesting;

import inventory.exception.RepositoryException;
import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class InventoryIntegrationTest {

    private InventoryService service;

    @BeforeEach
    void setUp() {
        Inventory inventory = new Inventory();
        InventoryRepository repository = new InventoryRepository(inventory, "test_data_file.txt");
        service = new InventoryService(repository);
    }

    @Test
    void addInhousePart() throws RepositoryException {
        service.addInhousePart("name", 1d, 5, 1, 10, 0);
        assertFalse(service.getAllParts().isEmpty());
        Part addedPart = service.getAllParts().get(0);
        assertEquals("name", addedPart.getName());
        assertEquals(1d, addedPart.getPrice(), 0.1);
        assertEquals(5, addedPart.getInStock());
        assertEquals(1, addedPart.getMin());
        assertEquals(10, addedPart.getMax());
    }

    @Test
    void deletePart() throws RepositoryException {
        assertTrue(service.getAllParts().isEmpty());
        service.addInhousePart("name", 1d, 5, 1, 10, 0);
        Part addedPart = service.getAllParts().get(0);
        int addedPartId = addedPart.getPartId();
        InhousePart partToDelete =
                new InhousePart(addedPartId, "name", 1d, 5, 1, 10, 0);
        assertFalse(service.getAllParts().isEmpty());
        service.deletePart(partToDelete);
        assertTrue(service.getAllParts().isEmpty());
    }
}
