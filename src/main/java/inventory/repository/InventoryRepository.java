package inventory.repository;


import inventory.exception.RepositoryException;
import inventory.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.util.StringTokenizer;

public class InventoryRepository {

	private static final Logger LOGGER = Logger.getLogger(InventoryRepository.class);
	private String filename;
	private Inventory inventory;

	public InventoryRepository(Inventory inventory, String filePath) {
		this.inventory = inventory;
		filename = filePath;
	}

	public InventoryRepository() throws RepositoryException {
		this.inventory=new Inventory();
		filename = "data/items.txt";
		readParts();
		readProducts();
	}

	private void readParts() throws RepositoryException {
		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		URL resource = classLoader.getResource(filename);
		if (resource == null) {
			return;
		}
		File file = new File(resource.getFile());
		ObservableList<Part> listP = FXCollections.observableArrayList();
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			while((line=br.readLine())!=null){
				Part part=getPartFromString(line);
				if (part!=null)
					listP.add(part);
			}
		} catch (FileNotFoundException e) {
			LOGGER.error("The file containing the data might be missing. The exception: " + e.getMessage());
			throw new RepositoryException("The file containing the data might be missing.");
		} catch (IOException e) {
			LOGGER.error("An error occurred while trying to fetch the data from the file. The exception: " + e.getMessage());
			throw new RepositoryException("An error occurred while trying to fetch the data from the file.");
		}
		inventory.setAllParts(listP);
	}

	private Part getPartFromString(String line){
		Part item=null;
		if (line==null|| line.equals("")) return null;
		StringTokenizer st=new StringTokenizer(line, ",");
		String type=st.nextToken();
		if (type.equals("I")) {
			int id= Integer.parseInt(st.nextToken());
			inventory.setAutoPartId(id);
			String name= st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			int idMachine= Integer.parseInt(st.nextToken());
			item = new InhousePart(id, name, price, inStock, minStock, maxStock, idMachine);
		}
		if (type.equals("O")) {
			int id= Integer.parseInt(st.nextToken());
			inventory.setAutoPartId(id);
			String name= st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			String company=st.nextToken();
			item = new OutsourcedPart(id, name, price, inStock, minStock, maxStock, company);
		}
		return item;
	}

	private void readProducts() throws RepositoryException {
		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		URL resource = classLoader.getResource(filename);
		if (resource == null) {
			return;
		}
		File file = new File(resource.getFile());

		ObservableList<Product> listP = FXCollections.observableArrayList();
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			while((line=br.readLine())!=null){
				Product product=getProductFromString(line);
				if (product!=null)
					listP.add(product);
			}
		} catch (FileNotFoundException e) {
			LOGGER.error("The file containing the data might be missing. The exception: " + e.getMessage());
			throw new RepositoryException("The file containing the data might be missing.");
		} catch (IOException e) {
			LOGGER.error("An error occurred while trying to fetch the data from the file. The exception: " + e.getMessage());
			throw new RepositoryException("An error occurred while trying to fetch the data from the file.");
		}
		inventory.setProducts(listP);
	}

	private Product getProductFromString(String line){
		Product product=null;
		if (line==null|| line.equals("")) return null;
		StringTokenizer st=new StringTokenizer(line, ",");
		String type=st.nextToken();
		if (type.equals("P")) {
			int id= Integer.parseInt(st.nextToken());
			inventory.setAutoProductId(id);
			String name= st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			String partIDs=st.nextToken();

			StringTokenizer ids= new StringTokenizer(partIDs,":");
			ObservableList<Part> list= FXCollections.observableArrayList();
			while (ids.hasMoreTokens()) {
				String idP = ids.nextToken();
				Part part = inventory.lookupPart(idP);
				if (part != null)
					list.add(part);
			}
			product = new Product(id, name, price, inStock, minStock, maxStock, list);
			product.setAssociatedParts(list);
		}
		return product;
	}

	private void writeAll() throws RepositoryException {

		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		URL resource = classLoader.getResource(filename);
		if (resource == null) {
			return;
		}
		File file = new File(resource.getFile());

		ObservableList<Part> parts=inventory.getAllParts();
		ObservableList<Product> products=inventory.getProducts();

		try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (Part p:parts) {
				LOGGER.info(p.toString());
				bw.write(p.toString());
				bw.newLine();
			}

			for (Product pr:products) {
				String line=pr.toString()+",";
				ObservableList<Part> list= pr.getAssociatedParts();
				int index=0;
				StringBuilder bld = new StringBuilder(line);
				while(index<list.size()-1){
					bld.append(list.get(index).getPartId()).append(":");
					index++;
				}
				line = bld.toString();
				if (index==list.size()-1)
					line=line+list.get(index).getPartId();
				bw.write(line);
				bw.newLine();
			}
		} catch (IOException e) {
			LOGGER.error("An error occurred while trying to write the data to the file. The exception: " + e.getMessage());
			throw new RepositoryException("An error occurred while trying to write the data to the file.");
		}
	}

	public void addPart(Part part) throws RepositoryException {
		inventory.addPart(part);
		writeAll();
	}

	public void addProduct(Product product) throws RepositoryException {
		inventory.addProduct(product);
		writeAll();
	}

	public int getAutoPartId(){
		return inventory.getAutoPartId();
	}

	public int getAutoProductId(){
		return inventory.getAutoProductId();
	}

	public ObservableList<Part> getAllParts(){
		return inventory.getAllParts();
	}

	public ObservableList<Product> getAllProducts(){
		return inventory.getProducts();
	}

	public Part lookupPart (String search){
		return inventory.lookupPart(search);
	}

	public Product lookupProduct (String search){
		return inventory.lookupProduct(search);
	}

	public void updatePart(int partIndex, Part part) throws RepositoryException {
		inventory.updatePart(partIndex, part);
		writeAll();
	}

	public void updateProduct(int productIndex, Product product) throws RepositoryException {
		inventory.updateProduct(productIndex, product);
		writeAll();
	}

	public void deletePart(Part part) throws RepositoryException {
		inventory.deletePart(part);
		writeAll();
	}

	public void deleteProduct(Product product) throws RepositoryException {
		inventory.removeProduct(product);
		writeAll();
	}

	public Inventory getInventory(){
		return inventory;
	}

	public void setInventory(Inventory inventory){
		this.inventory=inventory;
	}
}
