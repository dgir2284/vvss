package inventory.service;

import inventory.exception.RepositoryException;
import inventory.model.*;
import inventory.repository.InventoryRepository;
import javafx.collections.ObservableList;

public class InventoryService {

    private InventoryRepository repo;

    public InventoryService(InventoryRepository repo){
        this.repo =repo;
    }


    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue) throws RepositoryException {
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        repo.addPart(inhousePart);
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue) throws RepositoryException {
        OutsourcedPart outsourcedPart = new OutsourcedPart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        repo.addPart(outsourcedPart);
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts) throws RepositoryException {
        Product product = new Product(repo.getAutoProductId(), name, price, inStock, min, max, addParts);
        repo.addProduct(product);
    }

    public ObservableList<Part> getAllParts() {
        return repo.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return repo.getAllProducts();
    }

    public Part lookupPart(String search) {
        return repo.lookupPart(search);
    }

    public Product lookupProduct(String search) {
        return repo.lookupProduct(search);
    }

    public void updateInhousePart(int partIndex, InhousePart inhousePart) throws RepositoryException {
        repo.updatePart(partIndex, inhousePart);
    }

    public void updateOutsourcedPart(int partIndex, OutsourcedPart outsourcedPart) throws RepositoryException {
        repo.updatePart(partIndex, outsourcedPart);
    }

    public void updateProduct(int productIndex, Product product) throws RepositoryException {
        repo.updateProduct(productIndex, product);
    }

    public void deletePart(Part part) throws RepositoryException {
        repo.deletePart(part);
    }

    public void deleteProduct(Product product) throws RepositoryException {
        repo.deleteProduct(product);
    }

}
