package inventory.validator;

public class PartValidator {

    private PartValidator() {

    }

    /**
     * Generate an error message for invalid values in a part
     * Valid part will return an empty string
     * @param name - the name of the part
     * @param price - the price of the part
     * @param inStock - the number of parts in stock
     * @param min - the minimum number of parts accepted
     * @param max - the maximum number of parts accepted
     * @param errorMessage - the initial error message
     * @return - a constructed error message which contains all the found issues
     */
    public static String validate(String name, double price, int inStock, int min, int max, String errorMessage) {
        if(name.equals("")) {
            errorMessage += "A name has not been entered. ";
        }
        if(price < 0.01) {
            errorMessage += "The price must be greater than 0. ";
        }
        if(inStock < 1) {
            errorMessage += "Inventory level must be greater than 0. ";
        }
        if(min > max) {
            errorMessage += "The Min value must be less than the Max value. ";
        }
        if(inStock < min) {
            errorMessage += "Inventory level is lower than minimum value. ";
        }
        if(inStock > max) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }
        return errorMessage;
    }

}
