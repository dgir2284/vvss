package inventory.exception;

public class RepositoryException extends Exception {
    public RepositoryException(String message) {
        super(message);
    }
}
